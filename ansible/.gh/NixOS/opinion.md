https://www.reddit.com/r/zfs/comments/froqjn/best_server_os_for_zfs/flxcjj5/

NixOS is absolutely amazing and I never want an imperically configured system anymore. Since you've heard of it and especially since you have a friend who can help you get over the initial learning curve in the beginning, I'd definitely recommend you to try it out.

I have no idea what your friend means by it catching fire. They might have been describing the Arch-syndrome because NixOS can be hacked with infinite detail.
Though even if you did manage to break it, you can simply select a previous generation at boot and you're back to a working system (unless you manage to break your bootloader, that's just as severe as on most other distros).

Many of the core NixOS maintainers use ZFS on their systems (one even rolls back his root dataset to an empty snapshot on boot), so ZFS support is very good. When the kernel broke SIMD support for ZFS encryption and hashing last year, NixOS was the only distro that patched support back in for example (afaik).

For booting ZFS with native encryption I'd recommend you to use a separate FAT32 boot partition for /boot/ because you don't have to worry about ZFS support in GRUB and could even use an entirely different bootloader (all bootloaders support FAT32).
There's a gotcha about non-root pools for boot.zfs.requestEncryptionCredentials, so if you want those to be imported automatically you either need to put the keyfiles onto your root pool somewhere or use one or more LUKS device(s) as key files.
