# 4 Steps
- Check if integrity is preserved!
- remove device from vg & raid
- rebalance
- remove pv superblock & wipefs disk


# Fixes: pvcreate Device /dev/sdn excluded by a filter.
https://www.simplstor.com/index.php/support/support-faqs/118-lvm-dev-excluded-by-filter

`wipefs -a /dev/sdn`
