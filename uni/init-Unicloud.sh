# My vars
# You can change the $env and rerun! rest of script is Idempotent

env=default
uniroot=/home/$USER/.uni
inventory=$uniroot/cloud/ansible/inventories/$env


# --- Don't change anything after here!
uni(){
  git='https://gitlab.com/UniSocial.Net/UniCloud/cloud.git'
  gitdev='git@gitlab.com:UniSocial.Net/UniCloud/cloud.git'

  if [ ! -d "$uniroot/cloud" ]
  then
    mkdir -p $uniroot
    cd $uniroot
    git clone $git
    cd cloud
    git pull --recurse-submodules
  fi
}



link_env(){
  sudo chown -R $USER:$USER /etc/ansible
  ln -srf $inventory/default /etc/ansible/hosts
  ln -srf $inventory/group_vars /etc/ansible/
  ln -srf $inventory/host_vars /etc/ansible/
}

main(){
  install
  uni
  link_env
}
main
